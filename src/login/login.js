import React from "react"; 
import axios from 'axios';
import loginImg from "../login.svg";
import { Redirect } from 'react-router-dom';
import {parseJwt} from '../util/JWParser' ;
//import App from "../App";

 class login extends React.Component {
    constructor(propos)
    {               

        super(propos);
        const token =sessionStorage.getItem("token");
        let login = true;
        if (token == null){
            login=false
        }
        this.state=
        {value: '',
        password:'',
        log : false,
        login
        }
         

    this.handleChange = this.handleChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
        
    }

    handlePasswordChange(event) {
        this.setState({password: event.target.value});
     }

  
    handleSubmit(event) {
        event.preventDefault();
        axios.post("http://45.9.188.225:8080/cra/login",{pseudo: this.state.value,pwd: this.state.password})
          .then(response=> response.data)
          .then((data) =>{
            var decoded = parseJwt(data.token) ;
           // console.log("decoded",decoded);
              sessionStorage.setItem("token",data.token); 
              sessionStorage.setItem("role",decoded.role);    
              axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
              this.setState({log:true})
              
           // console.log(data)
          }).catch((error) => { console.log(error)
            this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
            Vos données de connexion sont erronées</p>
            this.setState({message:this.state.message}) })
      }

    render(){

        if(this.state.log){
            if(sessionStorage.getItem("redirectTo")){
                //console.log(sessionStorage.getItem("redirectTo"));
                var redirect = sessionStorage.getItem("redirectTo") ;
                // sessionStorage.removeItem("redirectTo") ;
                switch(redirect) {
                    case "profil" :  return <Redirect to ='/profil'/>
                    case "calendrier" : return <Redirect to = '/calendrier'/> 
                    default : return <Redirect to = '/calendrier'/> ;
                }

              
            } else {
                return <Redirect to = '/calendrier'/> ;
            }
        }
       
        return(
            <div>
                <nav className="navbar " width="100%"></nav>
                <div className="container" ref={this.props.containerRef}>
                    <div className="row">
                        <div className="col-lg-12">
                           
                            <div className="logform bg-light"> 
                                <img src={loginImg} width="150px" height="150px" alt="authentification"/>
                                {this.state.message != "" ? this.state.message : ""}
                                <form className="form" onSubmit={this.handleSubmit}>
                                    <div className="form-group">
                                        <label htmlFor="pseudo" className="lab">Pseudo</label>
                                        <input type="text" className="form-control" name="pseudo" value={this.state.value} onChange={this.handleChange} placeholder="votre pseudo"></input>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="pseudo">Mot de passe</label>
                                        <input type="password" name="pwd" className="form-control" value={this.state.pwd} onChange={this.handlePasswordChange} placeholder="votre mot de passe"></input>
                                    </div>
                                    <button type="submit" className="btn btn-primary btn-block" id="submit">Se connecter</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    }

export default login;