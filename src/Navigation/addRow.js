
import React from 'react';
import '../App.css';

function addRow() {
    var unixTimeZero = new Date('1 Jan 2020 00:00:00 GMT');
    var month_en = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    var currentDate = new Date(unixTimeZero.getTime() + 24 * 60 * 60 * 1000);
    var tasks = this.state.task.map((t) => t);
    var col1 = <td className="taches">selectionner votre activté
                        <select className="form-control">
            <option>Dev Cra</option>
        </select>
    </td>;
    var col2 = [];

    console.log(this.state.select)
    if (this.state.select == null) {
        for (var i = 0; i < this.state.colspan; i++) {
            var actual_month = new Date('1 ' + month_en[this.state.today.getMonth()] + ' 2020 00:00:00 GMT');
            var month_start = new Date(actual_month.getTime() + 24 * i * 60 * 60 * 1000);
            var dayofweek = (month_start.toString()).substring(0, 3);
            if ((dayofweek == 'Sat') || (dayofweek == 'Sun')) {
                col2.push(
                    <td><center>
                        <input type="text" disabled className="form-control cra" id="weekend" maxLength="1" pattern="[0-1]{1}"></input>
                    </center></td>);
            }
            else {
                col2.push(
                    <td><center>
                        <input type="text" className="form-control cra" maxLength="1" pattern="[0-1]{1}"></input>
                    </center></td>);
            }
        }

    }
    else {
        for (var y = 0; y < 25; y++) {
            if (y < 12) {
                var year = currentDate.getFullYear();
                var year_month = this.state.mois[y] + ' ' + year;

                if (this.state.select == year_month) {

                    var actual_month = new Date('1 ' + month_en[y] + ' ' + year + '  00:00:00 GMT');
                    var month = actual_month.getMonth();
                    this.state.colspan = this.state.nombre_jour[month]

                    for (var w = 0; w < this.state.nombre_jour[month]; w++) {
                        var currentMonth = new Date(actual_month.getTime() + 24 * w * 60 * 60 * 1000);
                        var day = currentDate.getDate() - 1 + w
                        var dayofweek = (currentMonth.toString()).substring(0, 3);

                        if ((dayofweek == 'Sat') || (dayofweek == 'Sun')) {
                            col2.push(
                                <td><center>
                                    <input type="text" disabled className="form-control cra" id="weekend" maxLength="1" pattern="[0-1]{1}"></input>
                                </center></td>);
                        }
                        else
                            col2.push(
                                <td><center>
                                    <input type="text" className="form-control cra" maxLength="1" pattern="[0-1]{1}"></input>
                                </center></td>);
                    }
                }
            }

            if (y > 12) {
                var year = currentDate.getFullYear() + 1;
                var year_month = this.state.mois[y - 13] + ' ' + year;

                if (this.state.select == year_month) {

                    var actual_month = new Date('1 ' + month_en[y - 13] + ' ' + year + '  00:00:00 GMT');
                    var month = actual_month.getMonth();
                    this.state.colspan = this.state.nombre_jour[month]

                    for (var z = 0; z < this.state.nombre_jour[month]; z++) {

                        var currentMonth = new Date(actual_month.getTime() + 24 * z * 60 * 60 * 1000);
                        var day = currentDate.getDate() - 1 + z
                        var dayofweek = (currentMonth.toString()).substring(0, 3);

                        if ((dayofweek == 'Sat') || (dayofweek == 'Sun')) {
                            col2.push(
                                <td><center>
                                    <input type="text" disabled className="form-control cra" id="weekend" maxLength="1" pattern="[0-1]{1}"></input>
                                </center></td>);
                        }
                        else {
                            col2.push(
                                <td><center>
                                    <input type="text" className="form-control cra" maxLength="1" pattern="[0-1]{1}"></input>
                                </center></td>);
                        }
                    }
                }
            }
        }
    }


    var col3 = <td>
        <input type="text" className="form-control" id="total" maxLength="2" disabled pattern="[0-1]{1}"></input>
    </td>
    tasks.push(<tr>{col1}{col2}{col3}</tr>)
    this.setState({ task: tasks });
}
