import React from 'react';
import {Bar} from 'react-chartjs-2';
import NavigationBar from './NavigationVar';
import axios from 'axios';
import { parseJwt } from '../util/JWParser';




export default class gestion_conges extends React.Component {
    constructor (){
        super();
        const token = sessionStorage.getItem("token");
        const decoded = parseJwt(token);
        var sysDate = new Date();
        var annee = sysDate.getFullYear();
        var mois = sysDate.getMonth()+1;
        var jour = sysDate.getDate();
        if (mois>9)
        {
          var today = annee+"-"+mois+"-"+jour
        }
        else if (mois <9)
        {
          var today = annee+"-0"+mois+"-"+jour
        }
       
        this.state= {
          decoded:decoded,
          up_conges:{},
          today:today,
          all_conges:[],
          conge_by_Id:{},
          delete_index:''
            }
      }

       getConges(){ 
         axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
         axios.get("http://45.9.188.225:8080/cra/Userconges/" + this.state.decoded.id)
         axios.get("http://45.9.188.225:8080/cra/AllConges/")
           .then(response => response.data)
           .then((data) => {
             console.log(data)
             this.setState({all_conges:data})
             this.state.all_conges.forEach(conge =>{
                 conge.duree=this.getBusinessDatesCount(new Date(conge.date_debut),new Date(conge.date_fin))-1+" jours"
         })
         this.setState({all_conges:this.state.all_conges})
        })
        }
      getUser(){ 
        axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
        //axios.get("http://45.9.188.225:8080/cra/Userconges/" + this.state.decoded.id)
        axios.get("http://45.9.188.225:8080/cra/user/")
          .then(response => response.data)
          .then((data) => {
            data.forEach(user=>
                {
                    user.conges.forEach(conge=>{
                        
                        conge.duree=this.getBusinessDatesCount(new Date(conge.date_debut),new Date(conge.date_fin))-1+" jours"
                        conge.demandeur = user.prenom + " "+ user.nom
                        this.state.all_conges.push(conge)
                        console.log(this.state.all_conges)
                    }
                        )
                   
                this.setState({all_conges:this.state.all_conges})
                })
         
          })
      }
      UpdateConge(){
        console.log(this.state.up_conges)
        axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
        axios.post("http://45.9.188.225:8080/cra/updateconges",this.state.up_conges)
           .then(response => response.data)
           .then((data) => {
             data.duree=
             this.getBusinessDatesCount
             (new Date(this.state.all_conges[this.state.delete_index].date_debut),new Date(this.state.all_conges[this.state.delete_index].date_fin))-1+" jours"
            this.state.all_conges[this.state.delete_index]=data
            console.log(this.state.delete_index)
            console.log(this.state.all_conges[this.state.delete_index])
            this.setState({all_conges:this.state.all_conges});
            }).catch((error) => { console.log(error) });
      }

      getBusinessDatesCount(startDate, endDate) {
        var count = 0;
        var curDate = startDate;
        while (curDate <= endDate) {
            var dayOfWeek = curDate.getDay();
            if(!((dayOfWeek == 6) || (dayOfWeek == 0)))
               count++;
            curDate.setDate(curDate.getDate() + 1);
        }
        return count;
        }
      componentDidMount(){
        this.getConges()
       // this.getUser()
      }
      handleSort(idconges,index,label){
        console.log(idconges)
          axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
          axios.get("http://45.9.188.225:8080/cra/conges/" + idconges )
            .then(response => response.data)
            .then((data) => {
                this.setState({up_conges:data})
                
             if (label=="Rejeté")
             { this.state.up_conges.etat_demande="Rejeté"
             this.state.up_conges.duree=
             this.getBusinessDatesCount(new Date(this.state.up_conges.date_debut),new Date(this.state.up_conges.date_fin))-1+" jours"
            }
             else if (label=="Validé")
             { this.state.up_conges.etat_demande="Validé"
             this.state.up_conges.duree=
             this.getBusinessDatesCount(new Date(this.state.up_conges.date_debut),new Date(this.state.up_conges.date_fin))-1+" jours"
             }
             console.log(this.state.up_conges)
             this.UpdateConge()
            })
            this.state.delete_index=index
            this.setState({delete_index:this.state.delete_index})
        }
      
     render() {
    
    return (
        <div>
          <NavigationBar/>
            <div className="container conges_container">
                <div className="row">
                    <div className="col-1"></div>
                    <div className="col-10">
                        <table className="table table-sm">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col"><i class="fas fa-user"></i> Demandeur</th>
                                <th scope="col"><i class="fas fa-spell-check"></i> Motif</th>
                                <th scope="col"><i class="fas fa-calendar-alt"></i> date début</th>
                                <th scope="col"><i class="fas fa-calendar-alt"></i> date fin</th>
                                <th scope="col"><i class="fas fa-info-circle"></i> status</th>
                                <th scope="col"><i class="fas fa-stopwatch"></i> durée</th>
                                <th scope="col"><i class="fas fa-cogs"></i> Actions</th>
                            </tr>
                            </thead>
                            <tbody>  
                            {this.state.all_conges.map((c,index) => (
                                
                                <tr>
                                
                                <td> {c.idconge}</td>
                                <td>{c.user.prenom} {c.user.nom}</td>
                                <td> {c.motif}</td>
                                <td>{c.date_debut}  </td>
                                <td>{c.date_fin}</td>
                                {c.etat_demande =="En cours"?
                                    <td className="btn btn-warning status"><i class="fas fa-hourglass-half"></i> {c.etat_demande}</td>
                                :c.etat_demande =="Validé"?
                                    <td className="btn btn-success status"><i class="fas fa-hourglass-half"></i> {c.etat_demande}</td>:
                                c.etat_demande =="Rejeté"?
                                    <td className="btn btn-danger status"><i class="fas fa-ban"></i> {c.etat_demande}</td>:<td>-</td>}
                                    <td>{c.duree}</td>
                                <td>
                                    
                                    <div class="btn-group">
                                    <button className="btn btn-success" data-toggle="modal"  onClick={() => this.handleSort(c.idconge,index,"Validé")} alt="valider cette demande" data-target="#exampleModalEdit"><i class="fas fa-check-circle"></i></button>
                                    <button className="btn btn-danger" data-toggle="modal" onClick={() => this.handleSort(c.idconge,index,"Rejeté")} data-target="#exampleModalDelete"><i class="fas fa-times-circle"></i></button>
                                    </div>
                                </td>
                                </tr>))}
                            </tbody>
                        </table>
            </div>
            <div className="col-1"></div>
            </div>
            </div>

        </div>
    
    )}
}