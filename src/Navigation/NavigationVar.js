import React from 'react';
import '../App.css';
import { Link } from 'react-router-dom';
import {parseJwt} from '../util/JWParser' ;


import { Redirect } from 'react-router-dom';
class NavigationBar extends React.Component {
  constructor(propos) {
    super(propos);
    this.state = {
      users: [],
      redirect: false,
      dropdown : [],
      init : false,
      conges:[]
    };
  }
  logout() {

    sessionStorage.removeItem("token")
    localStorage.removeItem("token")

  }
  componentDidMount() {


    var decoded = parseJwt(sessionStorage.getItem("token")) ;
    
    if(decoded.exp  < new Date().getTime() / 1000) {
      this.setState({redirect:true}) ;
    } else {
      this.setState({users:decoded}) ;
    }
    


  }

  render() {

    if (this.state.redirect) {
     
      sessionStorage.setItem("redirectTo",window.location.href.split("3000/")[1].split("?")[0]);

      return <Redirect to='/'/>

    }
   
      if((this.state.users.role=="Admin") &&(this.state.init===false)){
        this.state.dropdown.push(
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle navlibelle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Administration
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <Link to="users">
                <a class="dropdown-item" href="#">Gestion des utilisateurs</a>
              </Link>
              <Link to="projet">
              <a class="dropdown-item" href="#">Gestion des projets</a>
              </Link>
              <Link to="cra">
              <a class="dropdown-item" href="#">Consulter les CRA</a>
              </Link>
              <Link to="gestion_conges">
              <a class="dropdown-item" href="#">Gestion des conges</a>
              </Link>
            </div>
        </li>
      )
      this.setState({conges:[]})
      this.setState({init:true})
      }else if ((this.state.users.role!="Admin")){
        this.state.conges.push(<Link to="conges">
              <li className="nav-item">
                <a className="nav-link navlibelle" href="/#" id="logout">Gestion congés</a>
              </li>
            </Link>)     
      }
    
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-primary" id="navbar" width="100%">
        <a className="navbar-brand logo" href="/calendrier">       
          <img src="./Kertech.png" id="logo-nav" width="80px" height="25px"></img>
            </a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav col-4">
           
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle navlibelle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Gestion des CRA
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <Link to="calendrier">
              <a class="dropdown-item" href="#">Saisir CRA</a>
              </Link>
              <a class="dropdown-item" href="#">Consulter les CRA </a> 
            </div>
            </li>
            <Link to="profil">
              <li className="nav-item">
                <a className="nav-link navlibelle"  id="profil"href="/#">Profil</a>
              </li>
            </Link>
            {this.state.conges}
            <Link>
            {this.state.dropdown}
            </Link>
            
          </ul>
          <ul className="navbar-nav col-4"> </ul>
          <ul className="navbar-nav col-4">

            <Link to="/">
              <li className="nav-item">
                <a className="nav-link navlibelle" href="/#" id="logout" onClick={this.logout.bind(this)}>Déconnexion</a>
              </li>
            </Link>
            <li className="nav-item">
              <a className="nav-link navlibelle" id="user" href="/#" >{this.state.users.prenom} {this.state.users.nom} ({this.state.users.role})</a>
            </li>
            <Link to="pwd">
              <li className="nav-item">
                <a className="nav-link navlibelle" href="/#" id="user"><i class="fas fa-users-cog"></i></a>
              </li>
            </Link>
          </ul>
        </div>


      </nav>
      

    );
  }
}

export default NavigationBar;