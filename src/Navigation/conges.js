import React from 'react';
import {Bar} from 'react-chartjs-2';
import NavigationBar from './NavigationVar';
import axios from 'axios';
import { parseJwt } from '../util/JWParser';




export default class conges extends React.Component {


  
  constructor (){
    super();
    const token = sessionStorage.getItem("token");
    const decoded = parseJwt(token);
    var sysDate = new Date();
    var annee = sysDate.getFullYear();
    var mois = sysDate.getMonth()+1;
    var jour = sysDate.getDate();
    if (mois>9)
    {
      var today = annee+"-"+mois+"-"+jour
    }
    else if (mois <9)
    {
      var today = annee+"-0"+mois+"-"+jour
    }
   
    this.state= {
      decoded:decoded,
      up_conges:{},
      today:today,
      all_conges:[],
      conge_by_Id:{},
      delete_index:'',
      init: <button type="button" class="btn btn-lg btn-block btn-outline-primary" onClick={this.addConges.bind(this)}>
            Valider ma demande</button>,
       button : 'false'
        }
     
  }

  getConges(){ 
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    //axios.get("http://45.9.188.225:8080/cra/Userconges/" + this.state.decoded.id)
    axios.get("http://45.9.188.225:8080/cra/Userconges/"+this.state.decoded.id)
      .then(response => response.data)
      .then((data) => {
        this.setState({all_conges:data})
        this.state.all_conges.forEach(conge => {
          if (conge.etat_demande!='En cours')
          {
            conge.button='false'
          }
          else if (conge.etat_demande=='En cours')
          {
            conge.button=''
          }
        });
        this.setState({all_conges:this.state.all_conges})
        console.log(this.state.all_conges)
      })
     
  }

  addConges(){

    console.log("this.state.up_conges" ,this.state.up_conges);
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.post("http://45.9.188.225:8080/cra/addConges" , this.state.up_conges)
    .then(response => response.data)
    .then((data) => {
      this.state.up_conges = {
      motif: '',
      date_debut : '',
      date_fin : '',
      etat_demande : '',
      date_demande : '',
      description : '',
      }
      console.log(data)
      this.state.all_conges.push(data);
      this.setState({all_conges:this.state.all_conges});

    }).catch((error) => { console.log(error) 
      
      this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
       un probléme detetcté lors de l'insertion de votre nouvelle demande</p>
      this.setState({message:this.state.message})});
  }
  handleChange(label,e){
    if (e) {
      switch (label) {
        case 'motif': {
          this.state.up_conges.motif=e.target.value;
          this.setState({up_conges:this.state.up_conges})
        }
        break;
        case 'date_debut': {

          this.state.up_conges.date_debut=e.target.value;
          this.setState({up_conges:this.state.up_conges})
        }
        break;
        case 'date_fin': {

          this.state.up_conges.date_fin=e.target.value;
          this.setState({up_conges:this.state.up_conges})
        }
        break;
        case 'description': {

          this.state.up_conges.description=e.target.value;
          this.setState({up_conges:this.state.up_conges})
        }
        break;
       
        default: 
        break;

      }
      this.state.up_conges.date_demande = this.state.today;
      this.setState({up_conges:this.state.up_conges})
      this.state.up_conges.etat_demande = "En cours";
      this.setState({up_conges:this.state.up_conges})
      this.state.up_conges.user={"idemp":this.state.decoded.id}
      this.setState({up_conges:this.state.up_conges})
      console.log(this.state.up_conges)
    }
    
  }
  componentDidMount(){
    this.getConges()
    
  }
  DeleteConge(){
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
     axios.get("http://45.9.188.225:8080/cra/deleteconges/"+this.state.up_conges.idconge)
       .then(response => response.data)
       .then((data) => {
        this.state.all_conges.splice(this.state.delete_index,1);
        this.setState({all_conges:this.state.all_conges});
        }).catch((error) => { console.log(error) });
  
  }
handleSort(idconges,index,label){
  console.log(idconges)
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/conges/" + idconges )
      .then(response => response.data)
      .then((data) => {
          this.setState({up_conges:data})
      })
      this.state.delete_index=index
      this.setState({delete_index:this.state.delete_index})
      if (label=="edit")
      this.state.init = <button type="button" class="btn btn-lg btn-block btn-outline-success" onClick={this.UpdateConge.bind(this)}>
                        Editer ma demande</button>
  }
  UpdateConge(){
    console.log(this.state.up_conges)
    axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
    axios.post("http://45.9.188.225:8080/cra/updateconges",this.state.up_conges)
       .then(response => response.data)
       .then((data) => {
        this.state.up_conges = {
          motif: '',
          date_debut : '',
          date_fin : '',
          etat_demande : '',
          date_demande : '',
          description : '',
          }
        // this.state.all_conges.splice(this.state.delete_index,1);
        // this.state.all_conges.push(data);
        // this.setState({all_conges:this.state.all_conges});
        this.state.all_conges[this.state.delete_index]=data
        console.log(this.state.delete_index)
        console.log(this.state.all_conges[this.state.delete_index])
        this.setState({all_conges:this.state.all_conges});
        }).catch((error) => { console.log(error) });
  }
  render() {
    
    return (
      <div>
          <NavigationBar/>
          <div className="container conges_container">
          <div className="row">
            
            <div className="col-3">
            <div class="card-deck mb-3 text-center">
            <div class="card mb-4 shadow-sm">
              <div class="card-header bg-primary conges">
                <h4 class="my-0 font-weight-normal">Demander un congés</h4>
              </div>
              <div class="card-body">
                <form>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Type</label>
                    {/* <input type="text" class="form-control" value={this.state.up_conges.motif}  onChange={this.handleChange.bind(this, "motif")} placeholder="insérez le motif de l'absence"></input> */}
                    <select class="form-control" value={this.state.up_conges.motif}  onChange={this.handleChange.bind(this, "motif")}>
                      <option value=""></option>
                      <option value="CP">CP</option>
                      <option value="RTT">RTT</option>
                      <option value="Sans soldes">Sans soldes</option>
                      <option value="Autres">Autres</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Date de début</label>
                    <input type="date" class="form-control" value={this.state.up_conges.date_debut}  onChange={this.handleChange.bind(this, "date_debut")} placeholder="insérez le motif de l'absence"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Date de reprise</label>
                    <input type="date" class="form-control" value={this.state.up_conges.date_fin}  onChange={this.handleChange.bind(this, "date_fin")} placeholder="insérez le motif de l'absence"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Descrption (facultatif)</label>
                    <textarea id="story" name="story"  class="form-control" value={this.state.up_conges.description}  onChange={this.handleChange.bind(this, "description")}
                      rows="5" cols="33" maxlength="250" placeholder="Veuillez décrire votre demande de congé">
                        
                    </textarea>
                  </div>
                </form>
                {this.state.init}
                {/* <button type="button" class="btn btn-lg btn-block btn-outline-primary" onClick={this.addConges.bind(this)}>Enregistrer ma demande</button> */}
              </div>
            </div>
            </div>
            </div>
            <div className="col-9">
            <div class="card-deck mb-3 text-center">
            <div class="card mb-4 shadow-sm">
              <div class="card-header bg-primary conges">
                <h4 class="my-0 font-weight-normal">Mes demandes de congés</h4>
              </div>
              <div class="card-body">
              <table className="table table-sm">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col"><i class="fas fa-spell-check"></i> Type</th>
                <th scope="col"><i class="fas fa-calendar-alt"></i> date début</th>
                <th scope="col"><i class="fas fa-calendar-alt"></i> date fin</th>
                <th scope="col"><i class="fas fa-info-circle"></i> status</th>
                <th scope="col"><i class="fas fa-cogs"></i> Actions</th>
              </tr>
            </thead>
            <tbody>  
            {this.state.all_conges.map((c,index) => (
                
                <tr >
                  <td> {c.idconge}</td>
                  <td> {c.motif}</td>
                  <td>{c.date_debut}  </td>
                  <td>{c.date_fin}</td>
                  {c.etat_demande =="En cours"?
                      <td className="btn btn-warning status"><i class="fas fa-hourglass-half"></i> {c.etat_demande}</td>
                  :c.etat_demande =="Validé"?
                      <td className="btn btn-success status"><i class="fas fa-check-circle"></i> {c.etat_demande}</td>:
                   c.etat_demande =="Rejeté"?
                      <td className="btn btn-danger status"><i class="fas fa-times-circle"></i> {c.etat_demande}</td>:<td>-</td>}
                  <td>
                    <div class="btn-group">
                      <button className="btn btn-success" disabled={c.button} data-toggle="modal"  onClick={() => this.handleSort(c.idconge,index,"edit")}  data-target="#exampleModalEdit"><i class="fas fa-pencil-alt"></i></button>
                      <button className="btn btn-danger"  disabled={c.button} data-toggle="modal" onClick={() => this.handleSort(c.idconge,index,"delete")} data-target="#exampleModalDelete"><i class="fas fa-trash-alt"></i></button>
                    </div>
                  </td>
                </tr>))}
            </tbody>
            </table>
              </div>
            </div>
            </div>            
            </div>
          </div>
          </div>
          <div class="modal fade" id="exampleModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Suppresion demande congé</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              Êtes-vous sûr de vouloir Supprimer la demande de congé suivante : {this.state.up_conges.motif}
          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-block" onClick={this.DeleteConge.bind(this)} data-dismiss="modal">Confirmer la supression</button>
          <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
          </div>
        </div>
      </div>
    </div>   
      </div>

      
    );
  }
}