import React from 'react';
import '../App.css';
import NavigationBar from './NavigationVar';
import { Redirect } from 'react-router-dom';
import cra_data from '../data/CRA_data.json';
import logoKertech from '../logoKertech.json';
import axios from 'axios';
import { parseJwt } from '../util/JWParser';
import {Bar} from 'react-chartjs-2';


class checkUserTask extends React.Component {
    constructor(props) {
        super(props);
        this.state={
        users:[],
        option_month:[],
        option_year:[],
        affectation:[],
        selectedMonth:'',
        selectedYear:'',
        Month_converted_front:[],
        dayOfWork:[],
        nb_tache:[],
        consultant:[],
        infoTable:'',
        Pourcentage:'',
        style:'',
        month:''
       
        }
    }
    componentDidMount(){

        axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
        axios.get("http://45.9.188.225:8080/cra/user")
          .then(response => response.data)
          .then((data) => {
              this.setState({users:data})
              
          })
           
     }
     OptionsSelect() {
        
       
        
            var current_month_index = new Date().getMonth();
            for (var i = 0; i < 100; i++) {
                if (i < 12) {
                    this.state.month=i;
                    this.state.option_month.push(
                        <option value={this.state.month}>{cra_data[i].month}</option>
                    )
                }
                this.state.option_year.push(<option slected='false' value={parseInt(cra_data[0].year) + i}>{parseInt(cra_data[0].year) + i}</option>)
            }
        //this.setState({option_month:this.state.option_month})
    }
    
    handleChangeUser(event){
        // console.log(event.target.value)
       
    //    this.OptionsSelect()
    this.setState({selectedMonth:''})
        this.state.Pourcentage='';
        this.state.month='';
        this.setState({pr:this.state.Pourcentage})
        var today=new Date();
        var i = 0;
        var format_date= this.formatDate(today);
        this.state.developper = event.target.value
        this.setState({developper:this.state.developper});
        console.log(this.state.developper)
        axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
        axios.get("http://45.9.188.225:8080/cra/user/"+this.state.developper)
        .then(response => response.data)
        .then((data) => {
                this.setState({consultant:data})
                console.log(this.state.consultant)
        } 
                )
        axios.get("http://45.9.188.225:8080/cra/Tache/"+this.state.developper)
        .then(response => response.data)
        .then((data) => {                      
            this.setState({affectation:data})
            this.state.affectation.forEach(tasks => {
               var  month =this.convert_to_front(format_date,tasks.date_tache).getMonth(); 
                if (tasks.affectation && this.state.Month_converted_front.indexOf(month)==-1){
                    this.state.Month_converted_front.push(this.convert_to_front(format_date,tasks.date_tache).getMonth());} 
                    //this.state.dayOfWork.push(this.formatDate(this.convert_to_front(format_date,tasks.date_tache)))
                    i++;
                    console.log(tasks)
                    })
                    this.setState({dayOfWork:i})
                    
                   
                })
               
    }
    handleChangeMonth(event){
        
        // console.log(event.target.value)
        var today=new Date();
        var i = 0;
        var pr =0
        var format_date= this.formatDate(today);
        this.state.selectedMonth=event.target.value; 
        this.setState({selectedMonth:this.selectedMonth})
        var wd=this.WorkingDays(this.state.selectedMonth,2020)
        
        this.state.affectation.forEach(tasks => {
            if (this.convert_to_front(format_date,tasks.date_tache).getMonth()==event.target.value){
                i++;
                pr =((i*100)/wd).toFixed(2)
            }
            this.setState({dayOfWork:i})
            this.setState({Pourcentage:pr+"%" })
            
        })
        if (pr<25)
        {this.state.style="bas"}
        else if (pr>25 && pr<50)
        this.state.style="assezmoyen"
        else if (pr>50 && pr<70)
        this.state.style="moyen"
        else  if (pr>70) this.state.style="haut"
    }

    handleChangeYear(event){
        // console.log(event.target.value)
        this.state.selectedMonth=event.target.value; 
        this.setState({selectedYear:this.selectedYear})

    }

    convert_to_front(date,backDate) {
        
        var t = new Date(date);
        const localOffset2 = t.getTimezoneOffset() * 60000;
        var timestamp2 = (new Date(backDate).getTime() - localOffset2);
        var date = new Date(timestamp2); 
        return date;

    }

    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [year, month, day].join('-');
    }

    monthNameToNum(monthname) {
        var months = [
            'Janvier', 'Février', 'Mars', 'Avril', 'Mai',
            'Juin', 'Juillet', 'Août', 'Septembre',
            'Octobre', 'Novembre', 'Décembre'
        ];
        var month = months.indexOf(monthname);
        return month ? month + 1 : 0;
    }

    WorkingDays(month, year) {
        var days = 32 - new Date(year, month, 32).getDate();
        var weekdays = 0;

        for (var i = 0; i < days; i++) {
            var day = new Date(year, month, i + 1).getDay();
            if (day != 0 && day != 6) weekdays++;
        }
        return weekdays;
    }
    
render(){ 
   
    return(
        <div>
            <NavigationBar/>
            <div className="container">
                <div className="row view-cra">
                <div className="col-1"></div>
                    <div className="col-5">Veuillez selectionner un consultant</div>
                    <div className="col-5">Veuillez selectionner un mois</div>
                    <div className="col-1"></div>
                </div>
                <div className="row" onLoad={this.OptionsSelect()}>
                <div className="col-1"></div>
                    <div className="col-5">
                        <select className="form-control" onChange={this.handleChangeUser.bind(this)}>
                            <option></option>
                            {this.state.users.map((user,index) => (
                                <option value={user.idemp}> {user.prenom} {user.nom}</option>
                            ))}
                        </select>
                    </div>
                    <div className="col-5">
                        <select className="form-control" value={this.state.selectedMonth} onChange={this.handleChangeMonth.bind(this)}>
                            <option  value=""></option>
                            {this.state.option_month}
                        </select></div>
                    <div className="col-1"></div>
                </div>
                <div className="row view-cra">
                    <div className="col-1"></div>
                    <div className="col-10">
                <table className="table table-sm">
                                    <thead>
                                        <tr>
                                            <th>Consultant</th>
                                            <th>Nombre de jours travaillés</th>
                                            <th>Pourcentage</th>
                                        </tr>
                                    </thead>
                                    <thead>
                                        <tr>
                            <td>{this.state.consultant.prenom} {this.state.consultant.nom}</td>
                                            <td>{this.state.dayOfWork}</td>
                                            <td className={this.state.style}>{this.state.Pourcentage}</td>
                                        </tr>
                                    </thead>
                                    </table>
                </div>
                </div>
                <div className="col-1"></div>
            </div>
           
       
        </div>
    )
}
}
export default checkUserTask;