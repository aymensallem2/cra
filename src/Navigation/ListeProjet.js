import React from 'react';
import '../App.css';
import axios from 'axios';
import NavigationBar from './NavigationVar'

class ListeProjet extends React.Component {
    constructor(props) {
  
      super(props);
      this.state = {
        taches:[],
        TaskById:{},
        index:null,
        style:""
      };
    }
    handleChange(label, e) {
        if (e) {
          switch (label) {
            case 'libelle': {
    
              this.state.TaskById.libelle = e.target.value;
              this.setState({ TaskById: this.state.TaskById });
              console.log(this.state.TaskById);
            }
    
              break;
            case 'datelancement': {
              this.state.TaskById.datelancement = e.target.value;
              this.setState({ TaskById: this.state.TaskById });
            }
              break;
            case 'techno': {
            this.state.TaskById.techno = e.target.value;
            this.setState({ TaskById: this.state.TaskById });
            }
            break; 
            case 'client': {
            this.state.TaskById.client = e.target.value;
            this.setState({ TaskById: this.state.TaskById });
            }
            break; 
            case 'status': {
            this.state.TaskById.status = e.target.value;
            this.setState({ TaskById: this.state.TaskById });
            }
            break;        
            default: break;
          }
    
        }
      }
      componentDidMount() {
       
        axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
        axios.get("http://45.9.188.225:8080/cra/AllProject")
          .then(response => response.data)
          .then((data) => {
            this.setState({ taches: data });
            //console.log(data)
          });
      }

      AddTask(){
        this.state.TaskById.libelle = '';
        this.state.TaskById.datelancement = '';
        this.state.TaskById.client = '';
        this.state.TaskById.techno = '';
        this.state.TaskById.status = '';
        this.setState({ TaskById: this.state.TaskById })
      }
      ValidateTask() {
       
        axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
        axios.post("http://45.9.188.225:8080/cra/sProjet", this.state.TaskById)
    
          .then(response => response.data)
          .then((data) => {
            this.state.taches.push(data);
            this.setState({users:this.state.taches});
          }).catch((error) => { console.log(error) 
            this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
            un probléme detetcté lors de l'insertion de votre nouveau consultant</p>
            this.setState({message:this.state.message})
          });
      }

      DeleteTask(){
  
        axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
        axios.get("http://45.9.188.225:8080/cra/deleteProjet/"+this.state.TaskById.idprojet)
          .then(response => response.data)
          .then((data) => {
           this.state.taches.splice(this.state.index,1);
           this.setState({users:this.state.taches});
           }).catch((error) => { console.log(error) });
     }

     handleSort(idprojet,index) {
        axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
        axios.get("http://45.9.188.225:8080/cra/projet/" + idprojet)
          .then(response => response.data)
          .then((data) => {
            this.setState({ TaskById: data });
            console.log(this.state.TaskById)
            this.state.index=index ;
            
          });
      }
      UpdateTask(){

        axios.defaults.headers.common['Authorization'] = 'Bearer '+sessionStorage.getItem("token");
        console.log(this.state.userById)
        axios.post("http://45.9.188.225:8080/cra/updateProjet",this.state.TaskById)
          .then(response => response.data)
          .then((data) => {
           this.state.taches.splice(this.state.index,1);
           this.state.taches.push(data);
           this.setState({taches:this.state.taches});
           }).catch((error) => { console.log(error)
            this.state.message=<p className="error"><i class="fas fa-exclamation-circle"></i>
            votre modification est echouée</p>
            this.setState({message:this.state.message}) 
           });
     }
    render()
    
    {
        return (
        <div>
           <NavigationBar/>
            <div className="container">
            <div>
            <button class="btn btn-warning btn-lg addButton" data-toggle="modal" data-target="#exampleModalAdd" onClick={() => this.AddTask()}>
              <i className="fas fa-plus-circle"></i> Ajouter un nouveau projet
            </button>
            {this.state.message != "" ? this.state.message : ""}
          </div>
            <table className="table table-sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><i class="fas fa-spell-check"></i> Libelle</th>
                        <th><i class="fas fa-calendar-alt"></i> Date Lancement</th>
                        <th><i class="fas fa-building"></i> client</th>
                        <th><i class="fas fa-code"></i> technologies</th>
                        <th><i class="fas fa-info-circle"></i> Status</th>
                        <th><i class="fas fa-cogs"></i> Actions</th>
                    </tr>
                </thead>
                <tbody>
                {this.state.taches.map((tache,index) => (
                    <tr>
                        <td>{tache.idprojet}</td>
                        <td>{tache.libelle}</td>
                        <td>{tache.datelancement}</td>
                        <td>{tache.client}</td>
                        <td>{tache.techno}</td>
                        {tache.status =="Actif"?<td className="btn btn-success status"><i class="far fa-check-circle"></i> {tache.status}</td>
                        :tache.status =="Cloturé"?<td className="btn btn-danger status"><i class="fas fa-ban"></i> {tache.status}</td>:
                        <td>-</td>}
                        <td>                   
                        <div class="btn-group">
                        <button className="btn btn-primary" data-toggle="modal"  data-target="#exampleModal"><i class="fas fa-search"></i></button>
                        <button className="btn btn-success" data-toggle="modal" onClick={() => this.handleSort(tache.idprojet,index)} data-target="#exampleModalEdit"><i class="fas fa-pencil-alt"></i></button>
                        <button className="btn btn-danger" onClick={() => this.handleSort(tache.idprojet,index)}  data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-trash-alt"></i></button>
                        </div>
                        </td>
                    </tr>))}
                </tbody>
            </table>
            </div>
            <div class="modal fade bd-example-modal-lg" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel"> Ajouter un consultant </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >


                <form>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Libelle</label>
                    <input type="text" class="form-control" value={this.state.TaskById.libelle} onChange={this.handleChange.bind(this, "libelle")} placeholder="insérez le libelle du projet"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Date Lancement</label>
                    <input type="date" class="form-control" value={this.state.TaskById.datelancement} onChange={this.handleChange.bind(this, "datelancement")}></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Client</label>
                    <input type="text" class="form-control" value={this.state.TaskById.client} onChange={this.handleChange.bind(this, "client")}></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Technologie</label>
                    <input type="text" class="form-control" value={this.state.TaskById.techno} onChange={this.handleChange.bind(this, "techno")}></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Status</label>
                        <select className="form-control" value={this.state.TaskById.status} onChange={this.handleChange.bind(this, "status")}>
                            <option value=""></option>
                            <option value="Actif">Actif</option>
                            <option value="Cloturé">Cloturé</option>
                        </select>
                  </div>
                </form>
              </div>
              
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" onClick={this.ValidateTask.bind(this)} data-dismiss="modal">Ajouter le projet</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel"> Modifier un projet </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Libelle</label>
                    <input type="text" class="form-control" value={this.state.TaskById.libelle} onChange={this.handleChange.bind(this, "libelle")} placeholder="insérez le libelle du projet"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Date Lancement</label>
                    <input type="date" class="form-control" value={this.state.TaskById.datelancement} onChange={this.handleChange.bind(this, "datelancement")}></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Client</label>
                    <input type="text" class="form-control" value={this.state.TaskById.client} onChange={this.handleChange.bind(this, "client")}></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Technologie</label>
                    <input type="text" class="form-control" value={this.state.TaskById.techno} onChange={this.handleChange.bind(this, "techno")}></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Status</label>
                        <select className="form-control" value={this.state.TaskById.status} onChange={this.handleChange.bind(this, "status")}>
                            <option value=""></option>
                            <option value="Actif">Actif</option>
                            <option value="Cloturé">Cloturé</option>
                        </select>
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-success btn-block" onClick={this.UpdateTask.bind(this)} data-dismiss="modal">Enregistrer les modifications</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="exampleModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Suppresion consultant</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                Êtes-vous sûr de vouloir supprimer le projet : {this.state.TaskById.libelle}
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-block" onClick={this.DeleteTask.bind(this)} data-dismiss="modal">Confirmer la supression</button>
            <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
            </div>
          </div>
        </div>
      </div>
        </div>
        );}
}  
export default ListeProjet;