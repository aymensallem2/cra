import React from 'react';
import '../App.css';
import NavigationBar from './NavigationVar';
import { Redirect } from 'react-router-dom';
import { parseJwt } from '../util/JWParser';
import axios from 'axios';

class change_password extends React.Component {


  constructor(props) {

    super(props);
    const token = sessionStorage.getItem("token");
    let login = true;
    if (token == null) {
      login = false
    }
    this.state = {
      user: [],
      login,
      token: token,
      old: '',
      new: '',
      confirm: ''

    };
  }

  componentDidMount() {
    var decoded = parseJwt(sessionStorage.getItem("token"));
    if (decoded.exp < new Date().getTime() / 1000) {
      this.setState({ login: false });
    } else {


      axios.defaults.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem("token");
      axios.get("http://45.9.188.225:8080/cra/user/" + decoded.id)
        .then(response => response.data)
        .then((data) => {
          console.log(data)
          this.setState({ user: decoded })
        });
    }
  }

  getUserById() {

  }

  handleChange(label, e) {
    if (e) {
      switch (label) {
        case 'new_pass': {
          this.state.new = e.target.value
          this.setState({ new: this.state.new })
        }
          break;
        case 'old_pass': {
          this.state.old = e.target.value
          this.setState({ old: this.state.old })

        }
          break;
        case 'confirmed_pass': {
          this.state.confirm = e.target.value
          this.setState({ confirm: this.state.confirm })
        }
          break;
        default:
          break;

      }
    }

  }

  Update_pass() {
    var body = { "pseudo": this.state.user.pseudo, "oldPwd": this.state.old, "newPwd": this.state.new }
    if (this.state.old && this.state.new && this.state.confirm) {
      if (this.state.confirm == this.state.new) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem("token");
        axios.post("http://45.9.188.225:8080/cra/updatePassword", body)
          .then(response => response.data)
          .then((data) => {
            this.setState({
              old:'',
              new:'',
              confirm:''
            })
            sessionStorage.setItem("token", data.token)
            this.setState({
              message:
                <p className="success">
                  <i class="far fa-check-circle"></i> votre mot a été modifié avec succées</p>
            })
          }).catch((error) => {
              console.log(error)
            this.setState({
              message:
                <p className="error"><i class="fas fa-exclamation-circle"></i>
                  merci de vérifier votre mot de passe actuel</p>
            })
          })
      }
      else {
        this.setState({
          message:
            <p className="error"><i class="fas fa-exclamation-circle"></i>
                             les deux mots de passes ne sont pas identiques</p>
        })
      }
    } else {
      this.setState({
        message:
          <p className="warning"> <i class="fas fa-exclamation-triangle"></i>
         Merci de remplir tous les champs</p>
      })
    }
  }
  Reset(){
    this.setState({
      old:'',
      new:'',
      confirm:''
    })
  }
  render() {

    return (
      <div>
        <NavigationBar />
        <div className="container ">
          <div className="row changepwd">
            <div className="col-4"></div>
            <div className="col-4">
              <div class="card-deck mb-3 text-center">
                <div class="card mb-4 shadow-sm">
                  <div class="card-header bg-primary conges">
                    <h4 class="my-0 font-weight-normal">Changer mon mot de passe</h4>

                  </div>
                  <div class="card-body">
                    {this.state.message}
                    <form>
                      <div class="form-group">
                        <label for="exampleFormControlSelect1">Mot de passe actuelle</label>
                        <input type="password" required class="form-control" value={this.state.old} onChange={this.handleChange.bind(this, "old_pass")} placeholder="insérez votre mot de passe actuelle"></input>
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlSelect1">Nouveau mot de passe </label>
                        <input type="password" required value={this.state.new} class="form-control" onChange={this.handleChange.bind(this, "new_pass")} placeholder="insérez votre nouveau mot de passe"></input>
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlSelect1">Confirmer votre mot de passe</label>
                        <input type="password" required class="form-control" value={this.state.confirm} onChange={this.handleChange.bind(this, "confirmed_pass")} placeholder="confirmez votre mot de passe"></input>
                      </div>
                      <div class="button-group">

                        <input type="button" class="btn btn-success btn-block" onClick={this.Update_pass.bind(this)} value="Appliquer"></input>
                        <input type="reset" class="btn btn-secondary btn-block" onClick={this.Reset.bind(this)} value="Annuler"></input>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-4"></div>
          </div>
        </div>
      </div>);
  }
}

export default change_password;