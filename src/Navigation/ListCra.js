import React from 'react';
import '../App.css';
import axios from 'axios';
import NavigationBar from './NavigationVar'


class ListCra extends React.Component {
  constructor(props) {

    super(props);
    this.state = {

      users: [],
      userById: {
        // contrat:'',
        // role:'',

      },
      date: '',
      password: '',
      index: null,
      taches: [],
      task: [],
      projet: [],
      message: ''
    };
    // this.handleChange=this.handleChange.bind(this) ;
  }
  handleChange(label, e) {
    // this.setState({userById["pseudo"]:})

    if (e) {
      switch (label) {
        case 'date_embauche': {

          this.state.userById.date_embauche = e.target.value;
          this.setState({ userById: this.state.userById });
       
        }

          break;
        case 'pseudo': {
          this.state.userById.pseudo = e.target.value;
          this.setState({ userById: this.state.userById });
        }
          break;
        case 'nom': {
          this.state.userById.nom = e.target.value;
          this.setState({ userById: this.state.userById });
        }
          break;
        case 'prenom': {
          this.state.userById.prenom = e.target.value;
          this.setState({ userById: this.state.userById });
        }
          break;
        case 'email': {
          this.state.userById.email = e.target.value;
          this.setState({ userById: this.state.userById });
        }
          break;
        case 'telephone': {
          this.state.userById.telephone = e.target.value;
          this.setState({ userById: this.state.userById });
        }
          break;
        case 'adresse': {
          this.state.userById.adresse = e.target.value;
          this.setState({ userById: this.state.userById });
        }
          break;
        case 'contrat': {
          this.state.userById.contrat = e.target.value;
          this.setState({ userById: this.state.userById }); 
        }
          break;
        case 'num_secu': {
          this.state.userById.num_secu = e.target.value;
          this.setState({ userById: this.state.userById });
        }
          break;
        case 'mot_passe': {
          this.state.userById.pwd = e.target.value;
          this.setState({ userById: this.state.userById });
        }
        case 'role': {
          this.state.userById.role = e.target.value;
          this.setState({ userById: this.state.userById });
        }
          break;
        case 'projet': {

          var index = this.state.projet.findIndex((element) => { return element.idprojet == e.target.value });

          if (this.state.projet[index]) {
            if (this.state.projet[index].checked == '') {
              this.state.projet[index].checked = 'checked';
            }
            else {
              this.state.projet[index].checked = '';
            }

            this.setState({ projet: this.state.projet });
            this.state.taches = [];

            this.state.projet.forEach((proj, index) => {
              this.state.taches.push(
                <div className="col-1"><input type="checkbox" value={proj.idprojet} checked={this.state.projet[index].checked} onChange={this.handleChange.bind(this, "projet")} /></div>);
              this.state.taches.push(
                <div className="col-3"><label for="scales">{proj.libelle}</label></div>
              );
            });
          }

        }
          break;
        case 'salaire': {
          this.state.userById.salaire = e.target.value;
          this.setState({ userById: this.state.userById });
        }
          break;
        default:
          break;

      }

    }
  }
  AddUser() {
    this.state.userById = {
      pseudo: '',
      date_embauche: '',
      nom: '',
      prenom: '',
      adresse: '',
      email: '',
      contrat: '',
      num_secu: '',
      telephone: '',
      pwd: '',
      role: '',
      affectations: [],
      salaire: ''

    }
    // this.state.userById.pseudo = '';
    // this.state.userById.date_embauche = '';
    // this.state.userById.nom = '';
    // this.state.userById.prenom = '';
    // this.state.userById.adresse = '';
    // this.state.userById.email = '';
    // this.state.userById.contrat = '';
    // this.state.userById.num_secu = '';
    // this.state.userById.telephone = '';
    // this.state.userById.pwd = '';
    // this.state.userById.role = '';
    //this.state.userById.idemp = null ;
    this.setState({ userById: this.state.userById })



  }
  ValidateUser() {

    
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem("token");
    axios.post("http://45.9.188.225:8080/cra/register", this.state.userById)

      .then(response => response.data)
      .then((data) => {
        this.state.users.push(data);
        this.setState({ users: this.state.users });
      }).catch((error) => {
       

        this.state.message = <p className="error"><i class="fas fa-exclamation-circle"></i>
        un probléme detetcté lors de l'insertion de votre nouveau consultant</p>
        this.setState({ message: this.state.message })
      });
  }

  componentDidMount() {

    axios.defaults.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/user")
      .then(response => response.data)
      .then((data) => {
        this.setState({ users: data });

      });
  }
  print() {
    window.print();
  }

  handleSort(idemp, index) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/user/" + idemp)
      .then(response => response.data)
      .then((data) => {
        var date = [];
        var new_date;
        if (data.date_embauche) {
          date = (data.date_embauche).split('/');
          this.state.userById.date_embauche = date[2] + '-' + date[1] + '-' + date[0];
      
        }
        this.setState({ userById: data });
    
        this.state.index = index;
      }).then(() => {

        axios.get("http://45.9.188.225:8080/cra/AllProject")
          .then(response => response.data)
          .then((data) => {
            var row = []
            this.state.projet = data;

            this.state.projet.forEach((proj) => {
              proj.checked = this.state.userById.affectations.some((p) => { return p.projet.idprojet === proj.idprojet }) ? 'checked' : '';
              row.push(
                <div className="col-1"><input type="checkbox" value={proj.idprojet} checked={proj.checked} onChange={this.handleChange.bind(this, "projet")} /></div>);
              row.push(
                <div className="col-3"><label for="scales">{proj.libelle}</label></div>
              );
            });

            this.state.taches = row;
            this.setState({ projet: this.state.projet });
            this.setState({ taches: this.state.taches });
          });
      });
  }

  Size(obj) {
    var size = 0, key;
    for (key in obj) {
      size++;
    }
    return size;
  };

  DeleteUser() {

    axios.defaults.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem("token");
    axios.get("http://45.9.188.225:8080/cra/delete/" + this.state.userById.idemp)
      .then(response => response.data)
      .then((data) => {
        this.state.users.splice(this.state.index, 1);
        this.setState({ users: this.state.users });
      })
  }
  UpdateUser() {

    axios.defaults.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem("token");
    
    
    axios.post("http://45.9.188.225:8080/cra/update", this.state.userById)
      .then(response => response.data)
      .then((data) => {
        this.state.users.splice(this.state.index, 1);
        this.state.users.push(data);
      
        this.setState({ users: this.state.users });
      }).catch((error) => {
        
        this.state.message = <p className="error"><i class="fas fa-exclamation-circle"></i>
          votre modification est echouée</p>
        this.setState({ message: this.state.message })
      });
  }

  ValidateTask(e) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem("token");
    var checkedProject = this.state.projet.filter((p) => { return p.checked == "checked" })
    var body = [];
    if (checkedProject.length === 0) {
      body.push({ "user": { "idemp": this.state.userById.idemp } });
    }
    else {
      checkedProject.forEach((item) => {
        body.push({ "projet": { "idprojet": item.idprojet }, "user": { "idemp": this.state.userById.idemp } });
      });
    }
    axios.post("http://45.9.188.225:8080/cra/SaveAffectation/", body)
      .then(response => response.data)
      .then((data) => {
        this.state.userById = {};
        this.state.projet = [];
        
      })
    // });    

  }
  render() {
    return (
      <div>
        <NavigationBar />
        <div className="container">
          <div>
            <button class="btn btn-warning btn-lg addButton" data-toggle="modal" data-target="#exampleModalAdd" onClick={() => this.AddUser()}>
              <i className="fas fa-plus-circle"></i> Ajouter un nouveau utilisateur
            </button>
            {this.state.message != "" ? this.state.message : ""}

          </div>
          <table className="table table-sm">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col"><i class="fas fa-user"></i> Nom</th>
                <th scope="col"><i class="fas fa-address-card"></i> Pseudo</th>
                <th scope="col"><i class="fas fa-phone-alt"></i> Telephone</th>
                <th scope="col"><i class="fas fa-calendar-alt"></i> Date Embauche</th>
                <th scope="col"><i class="fas fa-cogs"></i> Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.state.users.map((user, index) => (

                <tr >
                  <td> {user.idemp}</td>
                  <td>{user.prenom} {user.nom} </td>
                  <td>{user.pseudo}</td>
                  <td>{user.telephone}</td>
                  <td>{user.date_embauche}</td>
                  <td>
                    <div class="btn-group">
                      <button className="btn btn-primary" data-toggle="modal" onClick={() => this.handleSort(user.idemp, index)} data-target="#exampleModal"><i class="fas fa-search"></i></button>
                      <button className="btn btn-success" data-toggle="modal" onClick={() => this.handleSort(user.idemp, index)} data-target="#exampleModalEdit"><i class="fas fa-pencil-alt"></i></button>
                      <button className="btn btn-warning" data-toggle="modal" onClick={() => this.handleSort(user.idemp, index)} data-target="#exampleModalTask"><i class="fas fa-thumbtack"></i></button>
                      <button className="btn btn-danger" data-toggle="modal" onClick={() => this.handleSort(user.idemp, index)} data-target="#exampleModalDelete"><i class="fas fa-trash-alt"></i></button>
                    </div>
                  </td>
                </tr>))}
            </tbody>
          </table>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Données du consultant: {this.state.userById.prenom} {this.state.userById.nom} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >
                <table className="table table-sm">
                  <tbody>
                    <tr>
                      <td><i class="fas fa-address-card"></i> Pseudo</td>
                      <td>{this.state.userById.pseudo}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-user"></i> Nom </td>
                      <td>{this.state.userById.prenom} {this.state.userById.nom}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-envelope"></i> Email</td>
                      <td>{this.state.userById.email}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-map-marked-alt"></i> Adresse</td>
                      <td>{this.state.userById.adresse}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-phone-alt"></i> Telephone</td>
                      <td>{this.state.userById.telephone}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-calendar-alt"></i> Date Embauche</td>
                      <td>{this.state.userById.date_embauche}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-file-signature"></i> Contrat</td>
                      <td>{this.state.userById.contrat}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-heartbeat"></i> Numéro Securité Sociale</td>
                      <td>{this.state.userById.num_secu}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-user-tag"></i> Role</td>
                      <td>{this.state.userById.role}</td>
                    </tr>
                    <tr>
                      <td><i class="fas fa-euro-sign"></i> Salaire</td>
                      <td>{this.state.userById.salaire}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel">Modifier les données de: {this.state.userById.prenom} {this.state.userById.nom} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >


                <form>
                  <div class="form-group">
                    <label for="exampleFormControlInput1">Pseudo</label>
                    <input required type="text" class="form-control" onChange={this.handleChange.bind(this, "pseudo")} value={this.state.userById.pseudo} placeholder="insérez le pseudo du consultant"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Nom</label>
                    <input required type="text" class="form-control" value={this.state.userById.nom} onChange={this.handleChange.bind(this, "nom")} placeholder="insérez le nom du consultant"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Prénom</label>
                    <input required type="text" class="form-control" value={this.state.userById.prenom} onChange={this.handleChange.bind(this, "prenom")} placeholder="insérez le prénom du consultant"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Email</label>
                    <input required type="email" class="form-control" value={this.state.userById.email} onChange={this.handleChange.bind(this, "email")} placeholder="insérez le email du consultant"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Adresse</label>
                    <input required type="email" class="form-control" value={this.state.userById.adresse} onChange={this.handleChange.bind(this, "adresse")} placeholder="insérez l'adresse du consultant"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Telephone</label>
                    <input required type="text" class="form-control" value={this.state.userById.telephone} onChange={this.handleChange.bind(this, "telephone")} placeholder="insérez le telephone du consultant"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Date Embauche</label>
                    <input required type="date" class="form-control" value={this.state.userById.date_embauche} onChange={this.handleChange.bind(this, "date_embauche")}></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Contrat</label>
                    <select required class="form-control" value={this.state.userById.contrat} onChange={this.handleChange.bind(this, "contrat")}>
                      <option value="">---------------------------------------------------------------------------------------------------</option>
                      <option value="CDI">CDI</option>
                      <option value="CDD">CDD</option>
                      <option value="Alternance">Alternance</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Role</label>
                    <select required class="form-control" value={this.state.userById.role} onChange={this.handleChange.bind(this, "role")} >
                      <option value="">---------------------------------------------------------------------------------------------------</option>
                      <option value="Admin">Admin</option>
                      <option value="Utilisateur">Utilisateur</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Num° Securité Sociale</label>
                    <input required type="text" class="form-control" value={this.state.userById.num_secu} onChange={this.handleChange.bind(this, "num_secu")} placeholder="insérez le numéro de sécurite sociale du consultant"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Salaire</label>
                    <input required type="text" class="form-control" value={this.state.userById.salaire} onChange={this.handleChange.bind(this, "salaire")} placeholder="insérez le salaire du consultant"></input>
                  </div>
                </form>


              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-success btn-block" onClick={this.UpdateUser.bind(this)} data-dismiss="modal">Enregistrer les modifications</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document" >
            <div class="modal-content">
              <div class="modal-header modalheader">
                <h5 class="modal-title" id="exampleModalLabel"> Ajouter un consultant </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal_items_position" >


                <form>
                  <div class="form-group">
                    <label for="exampleFormControlInput1">Pseudo</label>
                    <input type="text" class="form-control" onChange={this.handleChange.bind(this, "pseudo")} value={this.state.userById.pseudo} placeholder="insérez le pseudo du consultant"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Nom</label>
                    <input type="text" class="form-control" value={this.state.userById.nom} onChange={this.handleChange.bind(this, "nom")} placeholder="insérez le nom du consultant"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Prénom</label>
                    <input type="text" class="form-control" value={this.state.userById.prenom} onChange={this.handleChange.bind(this, "prenom")} placeholder="insérez le prénom du consultant"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Email</label>
                    <input type="email" class="form-control" value={this.state.userById.email} onChange={this.handleChange.bind(this, "email")} placeholder="insérez le email du consultant"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Adresse</label>
                    <input type="email" class="form-control" value={this.state.userById.adresse} onChange={this.handleChange.bind(this, "adresse")} placeholder="insérez l'adresse du consultant"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Telephone</label>
                    <input type="text" class="form-control" value={this.state.userById.telephone} onChange={this.handleChange.bind(this, "telephone")} placeholder="insérez le telephone du consultant"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Date Embauche</label>
                    <input type="date" class="form-control" value={this.state.userById.date_embauche} onChange={this.handleChange.bind(this, "date_embauche")}></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Contrat</label>
                    <select class="form-control" value={this.state.userById.contrat} onChange={this.handleChange.bind(this, "contrat")} >
                      <option value="">---------------------------------------------------------------------------------------------------</option>
                      <option value="CDI">CDI</option>
                      <option value="CDD">CDD</option>
                      <option value="Alternance">Alternance</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Role</label>
                    <select class="form-control" value={this.state.userById.role} onChange={this.handleChange.bind(this, "role")} >
                      <option value="">---------------------------------------------------------------------------------------------------</option>
                      <option value="Admin">Admin</option>
                      <option value="Utilisateur">Utilisateur</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Num° Securité Sociale</label>
                    <input type="text" class="form-control" value={this.state.userById.num_secu} onChange={this.handleChange.bind(this, "num_secu")} placeholder="insérez le numéro de sécurite sociale du consultant"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Mot de passe</label>
                    <input type="password" class="form-control" value={this.state.userById.pwd} onChange={this.handleChange.bind(this, "mot_passe")} placeholder="insérez un mot de passe pour le consultant"></input>
                  </div>
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Salaire</label>
                    <input required type="text" class="form-control" value={this.state.userById.salaire} onChange={this.handleChange.bind(this, "salaire")} placeholder="insérez le salaire du consultant"></input>
                  </div>
                </form>
              </div>
              <div class="modal-footer modalfooter">
                <button type="button" class="btn btn-primary btn-block" onClick={this.ValidateUser.bind(this)} data-dismiss="modal">Ajouter le consultant</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="exampleModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Suppresion consultant</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                Êtes-vous sûr de vouloir Supprimer le consultant : {this.state.userById.prenom} {this.state.userById.nom}
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-block" onClick={this.DeleteUser.bind(this)} data-dismiss="modal">Confirmer la supression</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="exampleModalTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Affecter des projets à : {this.state.userById.prenom} {this.state.userById.nom} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div className="row">
                    {this.state.taches}
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-block" onClick={this.ValidateTask.bind(this)} data-dismiss="modal">Finaliser l'affectation</button>
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default ListCra;