import React from 'react';
import '../App.css';
import NavigationBar from './NavigationVar';
import { Redirect } from 'react-router-dom';
import cra_data from '../data/CRA_data.json';
import logoKertech from '../logoKertech.json';
import axios from 'axios';
import { parseJwt } from '../util/JWParser';
import jsPDF from 'jspdf';


class calendar extends React.Component {

    init = true;
    total_saisie = 0;
    abs_total = 0;
    initMonth = true;
    index = 0;
    indexProjet = 0;
    constructor(props) {
        super(props);
        const token = sessionStorage.getItem("token");
        let login = true;
        let now = new Date();
        let year = now.getFullYear();
        var month = now.getMonth();
        var current_month = cra_data[month].month
        var decoded;
        var projets = [];


        if (token == null) {
            login = false
        }

        // else {
        decoded = parseJwt(token);
        //     decoded.affectations.map((a) => { a.projet.idaffectation = a.idaffectation; projets.push(a.projet) });


        // }
        this.state = {
            login,
            year: year,
            month: month,
            cases: [],
            absence: [],
            current_month: current_month,
            total: [],
            option_month: [],
            option_year: [],
            newRow: [],
            cra: [],
            abs: [],
            sum: [],
            days: cra_data[month].days,
            style: [],
            taches: [],
            somme: null,
            style: [],
            styleTotal: "form-control cra default_total",
            option_task: [],
            //projet: projets,
            projet: [],
            selectedProjet: [],
            projs: undefined,
            message: "",
            print_button: "",
            disableAll: false,
            isNotLogin: true,
            button_print: "",
            decoded: decoded,
            month_validate: [],
            AllAffectedTasks: [],
            nb_tache: [],
            affectation: [],
            dayOfWork: [],
            calendarBack: [],
            colonne: [],
            ligne: [],
            month_report: [],
            pafgeneration: true,
            valueSelect:['']

        }

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeAbs = this.handleChangeAbs.bind(this);
    }

    init_values() {

        // this.state.taches[this.index] = [];
        this.state.sum.forEach((e) => e = { value: 0 });


        this.state.total = [];

        this.state.days.forEach((d) => {
            this.state.sum.push({ value: '' })
            this.state.abs.push({ value: '' })
            this.state.cra.push({ value: '' })
            this.state.style.push({ value: 'form-control cra default_total' })
        });
    }

    handleChangeSelectedMonth(event) {
        this.componentWillMount();
        this.setState({valueSelect:['']});
        var mois = this.state.month_validate;
        let x = (mois) =>
            mois.filter((v, i) => mois.indexOf(v) === i)

        this.initMonth = false;
        this.index = 0;
        this.indexProjet = 0;
        this.state.taches = [];
        this.state.taches[this.index] = [];
        this.state.cases = [];
        this.state.total = [];
        this.state.sum.forEach((e) => e.value = '');
        this.state.abs.forEach((e) => e.value = '');
        this.state.style.forEach((e) => { e.value = 'form-control cra default_total' })

        this.init = true;


        if (this.state.month_validate.indexOf(parseInt(event.target.value)) >= 0) {
            this.setState({
                disableAll: true,
                button_print: <button className="btn btn-dark" onClick={this.generatepdf.bind(this)}>
                    <i class="fas fa-print"></i>
                    Imprimer vos CRAs
                    </button>,
                message: <p className="success">
                    <i class="far fa-check-circle"></i> Ce mois a  été validé avec succéss
                    </p>
            })
        }
        else {
            this.setState({
                disableAll: false,
                button_print: '',
                message: ''
            })
        }

        this.setState({
            year: cra_data[event.target.value].year,
            month: event.target.value,
            cases: [],
            absence: [],
            cra: [],
            current_month: cra_data[event.target.value].month,
            newRow: [],
            days: cra_data[event.target.value].days,
            total: [],
            taches: this.state.taches,
            sum: this.state.sum,
            abs: this.state.abs,
            somme: '',
            styleTotal: "form-control cra default_total",

        });

        var elements = document.getElementsByClassName("cra");

        for (var i = 0; i < elements.length; i++) {
            elements[i].value = '';
        }

        this.calendargenerator();

    }

    calculSommeColonne(index) {
        var tasksVal = this.state.taches.filter((ligne) => { return ligne.some((col) => col.value !== '') });
        var somme = 0;
        tasksVal.forEach((tache) => {
            somme = somme + (isNaN(parseInt(tache[index - 1].value)) ? 0 : parseInt(tache[index - 1].value));
        });
        somme = somme + (isNaN(this.state.abs[index - 1].value) ? 0 : this.state.abs[index - 1].value);
        this.state.sum[index - 1].value = somme == 0 ? '' : somme;


    }

    updateSommeTotale() {
        var somme_totale = 0;
        var totalVal = this.state.sum.filter((element) => { return element.value !== "" });
        totalVal.forEach((element) => { somme_totale = somme_totale + (isNaN(element.value) ? 0 : parseInt(element.value)); });
        this.state.somme = somme_totale === 0 ? '' : somme_totale;
        this.setState({ total: [] });
    }

    updateCSS(index) {
        var workingDays = this.WorkingDays(this.monthNameToNum(this.state.current_month) - 1, 2020)

        if (this.state.sum[index - 1].value !== '') {
            if ((this.state.sum[index - 1].value <= 1)) { this.state.style[index - 1].value = "form-control cra valid_total" }
            else { this.state.style[index - 1].value = "form-control cra invalid_total" }

        } else {
            { this.state.style[index - 1].value = "form-control cra default_total" }
        }

        if (this.state.sum[index - 1].value !== '') {
            if ((this.state.somme <= workingDays)) { this.state.styleTotal = "form-control cra valid_total" }
            else { this.state.styleTotal = "form-control cra invalid_total" }

        } else {
            { this.state.styleTotal = "form-control cra default_total" }
        }

    }

    updateTask(value, task) {
        this.init = false;
    
        task.value = isNaN(value) ? 0 : parseInt(value);


    }

    handleChange(index, ligne, event) {
  
        this.updateTask(event.target.value, this.state.taches[ligne][index - 1]);
        this.calculSommeColonne(index);
        this.updateSommeTotale();
        this.updateCSS(index);

    }

    handleChangeAbs(index, event) {

        if (event.target.value == 1) {
            this.abs_total++;
        }
        else if (event.target.value != 1) { this.abs_total = this.abs_total - 1; }
        this.updateTask(event.target.value, this.state.abs[index - 1]);
        this.calculSommeColonne(index);
        this.updateSommeTotale();
        this.updateCSS(index);
    }

    WorkingDays(month, year) {
        var days = 32 - new Date(year, month, 32).getDate();
        var weekdays = 0;

        for (var i = 0; i < days; i++) {
            var day = new Date(year, month, i + 1).getDay();
            if (day != 0 && day != 6) weekdays++;
        }
        return weekdays;
    }

    isWeekEnd(y, m, d) {
        const date = new Date(y + "/" + m + "/" + (parseInt(d)));
        const day = date.getDay();
        return ((day === 6) || (day === 0));   // 6 = Saturday, 0 = Sunday
    }

    handlechangeProjet(index, event) {
        this.init = false;
        this.state.valueSelect[index] = event.target.value ;
        this.setState({valueSelect:this.state.valueSelect});
        //cosole.log(event.target.value)
        var projet = []
        //cosole.log(this.state.affectation)
        //// //cosole.log(this.state.nb_tache)
        for (var i = 0; i < this.state.nb_tache.length; i++) {
            projet.push({ 'id': event.target.value, 'Libelle': this.state.affectation[i].projet.libelle })
        }
        this.state.taches[index].idAffectation = event.target.value;
        //cosole.log("this.state.taches[index]",this.state.taches[index])
        for (var i = 0; i < this.state.nb_tache.length; i++) {
            if (event.target.value == this.state.affectation[i].idaffectation) {
                this.state.taches[index].LibelleAffectation = projet[i].Libelle;
            }
        }
       // this.setState({taches:this.state.taches})
    }


    OptionsSelect() {

        if (this.initMonth === true) {
            var current_month_index = new Date().getMonth();
            for (var i = 0; i < 100; i++) {
                if (i < 12) {
                    this.state.option_month.push(
                        <option selected={current_month_index === i} value={i}>{cra_data[i].month}</option>
                    )
                }
                this.state.option_year.push(<option value={parseInt(cra_data[0].year) + i}>{parseInt(cra_data[0].year) + i}</option>)
            }
        }
    }

    loadAll() {
        // this.getProjects()
        // //if (!this.state.projs) {
        //     // //cosole.log(this.state.projet)
        //     var options = [];
        //     this.state.projet.forEach((p) => {
        //         options.push(<option value={p.idaffectation}>{p.libelle}</option>);
        //     });
        //     var projs = <select className="form-control" onChange={this.handlechangeProjet.bind(this, 0)}>
        //         <option value=""> </option>
        //         {options}
        //     </select>;
        //     this.setState({ projs: projs })
        // //}

    }

    calendargenerator() {

        var m = this.state.month;
        var year = cra_data[m].year;
        if (this.init == true) {
            this.init_values();
            this.state.taches[this.index] = [];
        }

        this.state.days.forEach(day => {

            var isDisabled = this.isWeekEnd(year, (parseInt(m) + 1), day.day) || this.state.disableAll;

            if (this.init == true) {
                this.state.taches[this.index].push({ day: day.day, value: '' });
            }
            const classes = "form-control cra " + ((this.isWeekEnd(year, (parseInt(m) + 1), day.day)) ? 'isWeekEnd' : '');
            this.state.cases.push(
                <td ><center>{day.day}

                    <input type="text" disabled={isDisabled} onChange={this.handleChange.bind(this, day.day, this.index)} className={classes} maxLength="1" pattern="[0-1]{1}"></input>
                </center></td>
            );
            this.state.absence.push(
                <td><center>
                    <input type="text" disabled={isDisabled} onChange={this.handleChangeAbs.bind(this, day.day)} className={classes} maxLength="1" pattern="[0-1]{1}"></input>
                </center></td>);
            this.state.total.push(<td><center><input type="text" value={this.state.sum[day.day - 1].value} disabled="true" id={day.day} className={this.state.style[day.day - 1].value} maxLength="1" pattern="[0-1]{1}"></input></center></td>);
        });
    }

    AddRow() {
        this.index++;
        this.indexProjet++;

        this.init = true;

        this.initMonth = false;
        var m = this.state.month;
        var year = cra_data[m].year;
        this.state.newRow = this.state.newRow.map((t) => t);
        let col1 = <td className="taches">
            <select className="form-control" value = {this.state.valueSelect[this.index]}disabled={this.state.disableAll} onChange={this.handlechangeProjet.bind(this, this.indexProjet)}>
                <option value=""> </option>
                {this.state.affectation.map((a, index) => (
                    <option value={a.idaffectation}>{a.projet.libelle}</option>
                ))}
            </select>
        </td>;
        var col2 = [];
        cra_data[m].days.forEach(day => {
            var isDisabled = this.isWeekEnd(year, (parseInt(m) + 1), day.day) || this.state.disableAll;
            const classes = "form-control cra tache " + ((this.isWeekEnd(year, (parseInt(m) + 1), day.day)) ? 'isWeekEnd' : '');
            const input = <td><center><input type="text" disabled={isDisabled} onChange={this.handleChange.bind(this, day.day, this.index)} className={classes} maxLength="1" pattern="[0-1]{1}"></input></center></td>
            col2.push(input);
        });


        this.state.newRow.push(<tr>{col1}{col2}</tr>);
        this.setState({ newRow: this.state.newRow });

    }

    Size(obj) {
        var size = 0, key;
        for (key in obj) {
            size++;
        }
        return size;
    };

    monthNameToNum(monthname) {
        var months = [
            'Janvier', 'Février', 'Mars', 'Avril', 'Mai',
            'Juin', 'Juillet', 'Août', 'Septembre',
            'Octobre', 'Novembre', 'Décembre'
        ];
        var month = months.indexOf(monthname);
        return month ? month + 1 : 0;
    }

    ValidateTask() {
        //cosole.log(this.state.taches)
        var daily_sum = true
        var workingDays = this.WorkingDays(this.monthNameToNum(this.state.current_month) - 1, 2020)
        var bodyTask = [];
        var total_distinct_insertion = this.abs_total + bodyTask.length
        if (this.state.somme > workingDays) {
            this.state.message = <p className="error"><i class="fas fa-exclamation-circle"></i>
                 Merci de Vérifier que la somme de toutes les journées est inférieur ou égale à 1</p>;
            this.setState({ message: this.state.message })


        }
        else if (daily_sum === false) {
            this.state.message = <p className="error"><i class="fas fa-exclamation-circle"></i>
                 Merci de Vérifier que la somme de toutes les journées est inférieur ou égale à 1</p>;
            this.setState({ message: this.state.message })



        }
        else if (daily_sum === true) {


            if (this.state.somme < workingDays) {
                this.state.message = <p className="warning">
                    <i class="fas fa-exclamation-triangle"></i>
                 Merci de remplir toutes les journées avant de valider vos CRAs
                </p>;
                this.setState({ message: this.state.message })
                total_distinct_insertion = 0

            }
            
            else if (this.state.somme == workingDays) {

                for (var i = 0; i < this.state.taches.length; i++) {
                    for (var j = 0; j < this.state.days.length; j++) {
                        var date = "2020-" + this.monthNameToNum(this.state.current_month) + "-" + this.state.taches[i][j].day;
                        var actual_date = (new Date(date).getTime()) / 1000
        
                        if (this.state.taches[i][j].value ===1) {

                            
                            this.total_saisie++;
                            
                            if (this.state.sum[j].value > 1) {     
                                daily_sum = false
                            }
                            else {
                                bodyTask.push({ "affectation": { "idaffectation": this.state.taches[i].idAffectation || 0 }, "date_tache": actual_date });
                                  //cosole.log(this.state.taches[i].idAffectation)  
                            }
                        } 

                    }
                }
                
                axios.post("http://45.9.188.225:8080/cra/SaveTache/", bodyTask)
                    .then(response => response.data)
                    .then((data) => {
                        
                        //this.SessionSave()
                        
                        this.state.disableAll = true
                        this.state.message = <p className="success">
                            <i class="far fa-check-circle"></i> Validation éffectué avec succéss
                    </p>;
                        this.setState({ disableAll: true });
                        this.setState({ message: this.state.message });
                        this.setState({ cases: [] });
                        this.setState({ absence: [] });
                        var jours = document.getElementsByClassName("tache");
                        for (var i = 0; i < jours.length; i++) {
                            document.getElementsByClassName("tache")[i].setAttribute("disabled", true);
                        }
                        this.state.button_print = <button className="btn btn-dark" onClick={this.generatepdf.bind(this)}>
                            <i class="fas fa-print"></i>
                        Imprimer vos CRAs
                        </button>
                        this.setState({ button_print: this.state.button_print });
                    }).catch((error) => {
                        this.state.message = <p className="error">
                            <i class="fas fa-exclamation-circle"></i>
                    Veuillez choisir une tache
                </p>;
                        // //cosole.log(error);
                        this.setState({ message: this.state.message });
                        this.setState({ disableAll: false });
                        
                    });

            }
        }

        this.total_saisie = 0;
    }

    generatepdf() {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem("token");
        axios.get("http://45.9.188.225:8080/cra/Tache/" + this.state.decoded.id)
            .then(response => response.data)
            .then((data) => {
                var today = new Date();
                var format_date = this.formatDate(today);
                var unique = []
                
                this.setState({ AllAffectedTasks: data });
                this.state.AllAffectedTasks.forEach(tasks => {
                    if (tasks.affectation && this.state.month_validate.indexOf(this.convert_to_front(format_date, tasks.date_tache).getMonth()) == -1) {
                        this.state.month_validate.push(this.convert_to_front(format_date, tasks.date_tache).getMonth());
                    }
                    this.state.dayOfWork.push(this.formatDate(this.convert_to_front(format_date, tasks.date_tache)))
                    if (tasks.affectation && this.state.nb_tache.indexOf(tasks.affectation.idaffectation) == -1) {
                        this.state.nb_tache.push(tasks.affectation.idaffectation);
                    }
                })
                this.setState({ month_validate: this.state.month_validate });
                //// //cosole.log(this.state.nb_tache)
                // //cosole.log(this.state.month_validate.indexOf(parseInt(this.monthNameToNum(this.state.current_month) - 1)))
            }).then(() => {
                this.ValidateCalendarGenerator();
            }).then(() => {
                var pdf = new jsPDF('p', 'pt', 'letter');
                pdf.addImage(logoKertech.base64, 'JPG', 275, 10, 50, 50)
                var final_tab = []
                var days_of_work = 0;
                var nb_weekend = 0;
                var workingDays = this.WorkingDays(this.monthNameToNum(this.state.current_month) - 1, 2020)
                //     if (this.state.pafgeneration===true){
                //     for (var j = 0; j < this.state.days.length; j++) {
                //             var  weekend=this.isWeekEnd(2020, (parseInt(this.monthNameToNum(this.state.current_month)) ), j+1)
                //             if(!weekend){ 
                //             for (var i = 0; i < this.state.taches.length; i++) {
                //                 if (this.state.taches[i][j].value==1){final_tab.push(this.state.taches[i].LibelleAffectation);days_of_work++;}
                //             }
                //                     if (this.state.abs[j].value==1){final_tab.push('Absence')}}
                //             else if(weekend) {final_tab.push('-');nb_weekend++}
                //     }
                // }
                // else if (this.state.pafgeneration===false){
                if (final_tab.length == nb_weekend) {
                    final_tab = this.state.month_report
                }
                // //cosole.log(final_tab)
                var dayss = this.state.month_report.filter((element) => { return element != "-" && element != "Absence" }).length
                // //cosole.log(dayss)

                //  if (this.state.month_report.length>0){
                //  for (var i=0;i<this.state.days.length;i++)
                //      {
                //          if (this.state.month_report[i]=="Absence"){
                //              dayss=dayss
                //      }
                //      else if (this.state.month_report[i]=='-'){
                //          dayss=dayss
                //      }
                //      else (dayss++)
                //  }
                //  days_of_work=dayss}
                // }
                // //cosole.log(days_of_work)
                var y = 15; var x = 1;
                for (var i = 0; i < this.state.days.length; i++) {
                    pdf.setFontSize(12);
                    pdf.setFont("courier");
                    if (final_tab[i] == '-') {
                        pdf.setTextColor(255, 0, 0)
                        pdf.text(350, 150 + (y * x), final_tab[i])
                    }
                    else if (final_tab[i] != '-') {
                        pdf.setTextColor(0, 0, 0)
                        pdf.text(350, 150 + (y * x), final_tab[i])
                    }
                    pdf.text(100, 150 + (y * x), (x) + ' ' + this.state.current_month + ' ' + '2020 ');
                    x++;
                }

                pdf.cellInitialize();

                pdf.setFontSize(13);
                pdf.setFont("courier");
                pdf.setTextColor(0, 0, 0)
                pdf.setFontType('bold')
                pdf.text(50, 90, 'Mois : ')
                pdf.setFontType('normal')
                pdf.text(150, 90, this.state.current_month + ' 2020 ')
                pdf.setFontType('bold')
                pdf.text(350, 90, 'Employé:')
                pdf.setFontType('normal')
                pdf.text(450, 90, this.state.decoded.prenom + ' ' + this.state.decoded.nom.toUpperCase())
                pdf.setFontType('bold')
                pdf.text(350, 50, 'jours travaillés:')
                pdf.setFontType('normal')

                pdf.text(500, 50, dayss + '/' + workingDays)

                pdf.setLineWidth(1)
                pdf.line(25, 110, 575, 110)
                pdf.setFontSize(10);
                pdf.setLineWidth(0.5)
                pdf.line(25, 650, 575, 650)
                pdf.setFontType('bold')
                pdf.text(50, 700, 'Signature client:')
                pdf.setFontType('bold')
                pdf.text(450, 700, 'Signature consultant:')
                pdf.text(350, 675, this.System_Date())
                pdf.save('CRA_' + this.state.current_month + '_2020' + this.state.decoded.prenom + ' ' + this.state.decoded.nom + '.pdf');

            })


    }

    System_Date() {

        var date = new Date()
        var year = date.getFullYear()
        var month = date.getMonth() + 1
        var jour = date.getDate()
        var heure = date.getHours()
        var minutes = date.getMinutes();
        var sys_date = 'document géneré le : ' + jour + '-' + month + '-' + year + ' à ' + heure + ':' + minutes;
        return sys_date
    }

    componentDidMount() {

        this.OptionsSelect();

    }

    convert_to_front(date, backDate) {

        var t = new Date(date);
        const localOffset2 = t.getTimezoneOffset() * 60000;
        var timestamp2 = (new Date(backDate).getTime() - localOffset2);
        var date = new Date(timestamp2);
        return date;

    }

    format(date) {
        return date.split('T')[0] + " " + date.split('T')[1].split(':')[0] + ":" + date.split('T')[1].split(':')[1];
    }

    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }

    componentWillMount() {
        var today = new Date();
        var format_date = this.formatDate(today);
        var unique = []

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem("token");
        axios.get("http://45.9.188.225:8080/cra/Tache/" + this.state.decoded.id)
            .then(response => response.data)
            .then((data) => {
                

                this.setState({ AllAffectedTasks: data });
                this.state.AllAffectedTasks.forEach(tasks => {
                    if (tasks.affectation && this.state.month_validate.indexOf(this.convert_to_front(format_date, tasks.date_tache).getMonth()) == -1) {
                        this.state.month_validate.push(this.convert_to_front(format_date, tasks.date_tache).getMonth());
                    }
                    this.state.dayOfWork.push(this.formatDate(this.convert_to_front(format_date, tasks.date_tache)))
                    if (tasks.affectation && this.state.nb_tache.indexOf(tasks.affectation.idaffectation) == -1) {
                        this.state.nb_tache.push(tasks.affectation.idaffectation);
                    }
                })
                this.setState({ month_validate: this.state.month_validate });
                //// //cosole.log(this.state.nb_tache)
                // //cosole.log(this.state.month_validate.indexOf(parseInt(this.monthNameToNum(this.state.current_month) - 1)))
                if (this.state.month_validate.indexOf(parseInt(this.monthNameToNum(this.state.current_month) - 1)) != -1) {
                    // this.state.disableAll=true;
                    this.setState({ disableAll: true });
                    this.setState({ cases: [] })
                    this.setState({ absence: [] })
                    this.setState({
                        button_print: <button className="btn btn-dark" onClick={this.generatepdf.bind(this)}>
                            <i class="fas fa-print"></i>
                 Imprimer vos CRAs
                 </button>

                    })
                    this.setState({
                        message:
                            <p className="success">
                                <i class="far fa-check-circle"></i> Ce mois a  été validé avec succéss
                    </p>
                    })
                }
                else {
                    this.setState({ disableAll: false });
                    this.setState({ cases: [] })
                    this.setState({ absence: [] })
                    // //cosole.log(this.state.current_month)
                }

                this.setState({ pafgeneration: false })
                this.ValidateCalendarGenerator()
            });

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem("token");
        axios.get("http://45.9.188.225:8080/cra/getAffectationByUser/" + this.state.decoded.id)
            .then(response => response.data)
            .then((data) => {
                this.state.affectation = data;
                this.setState({ affectation: this.state.affectation })
                // //cosole.log(this.state.affectation)
            })

    }

    ValidateCalendarGenerator() {
        var today = new Date();
        var format_date = this.formatDate(today);

        var date_front_year = '';
        var date_front_month = '';
        var date_front_day = '';
        var date_front = '';
        var workdays = []
        var final_tab = []
        var m = parseInt(this.monthNameToNum(this.state.current_month))
        var m_loop = this.monthNameToNum(this.state.current_month) - 1
        this.state.AllAffectedTasks.forEach(tasks => {
            date_front_year = this.convert_to_front(format_date, tasks.date_tache).getFullYear();
            date_front_month = this.convert_to_front(format_date, tasks.date_tache).getMonth() + 1;
            date_front_day = this.convert_to_front(format_date, tasks.date_tache).getDate();
            date_front = date_front_year + "-" + date_front_month + "-" + date_front_day
            if (date_front_month === this.monthNameToNum(this.state.current_month))
                workdays.push(date_front)


        })
        //// //cosole.log(this.state.AllAffectedTasks)
        // //cosole.log(workdays)
        var i = 0;
        for (var j = 0; j < this.state.days.length; j++) {

            var date = 2020 + '-' + m + '-' + (j + 1)
            //// //cosole.log(workdays.indexOf(date))
            if (workdays.indexOf(date) == -1) {
                if (this.isWeekEnd(2020, m, (j + 1))) final_tab.push("-")
                else final_tab.push("Absence")
            }
            else {
                final_tab.push(this.state.AllAffectedTasks[i].affectation.projet.libelle)
                i++;
                // //cosole.log(i)
            }

        }
        // //cosole.log(this.state.AllAffectedTasks)
        //// //cosole.log(final_tab)
        // //cosole.log(final_tab)
        this.setState({ month_report: final_tab })
    }


    render() {
        this.calendargenerator();
        //this.ValidateCalendarGenerator();  
        if (this.state.login === false) {
            return <Redirect to="/" />
        }
        return (
            <div>
                <NavigationBar />
                <div className="container" >
                    <div className=" row lmonth" >
                        <div className="col-2">Mois : </div>
                        <div className="col-3">
                            <select className="form-control" onChange={this.handleChangeSelectedMonth.bind(this)}>

                                {this.state.option_month}
                            </select></div>
                        <div className="col-3">Année :
                                </div>
                        <div className="col-3">
                            <select className="form-control" >
                                <option>
                                    2020
                                    </option>
                                {/* {this.state.option_year} */}
                            </select>
                        </div>
                    </div>
                    <div className="footer">
                        <button className="btn btn-warning" disabled={this.state.disableAll} onClick={this.AddRow.bind(this)}>
                            <i className="fas fa-plus-circle"></i> Ajouter une nouvelle tache
                    </button>&nbsp;
                    <button className="btn btn-success" disabled={this.state.disableAll} onClick={this.ValidateTask.bind(this)}>
                            <i className="fas fa-paper-plane" ></i> Valider ma feuille de temps
                    </button>&nbsp;
                    {this.state.button_print != "" ? this.state.button_print : ""}
                        {this.state.message != "" ? this.state.message : ""}
                    </div>

                    <div className="cra"    >

                        <form id='form'>
                            <table className="table table-sm" id="tblTask" >
                                <thead >
                                    <tr>
                                        <th>Tâches</th>
                                        <th colSpan={this.state.days.length} >{this.state.current_month} {this.state.year}</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td className="col_fix" disabled={this.state.disableAll}>Selectionner votre activté
                                            {/* {this.state.projs} */}
                                            <select value = {this.state.valueSelect[0]} className="form-control" onChange={this.handlechangeProjet.bind(this, 0)}>
                                                <option value=""> </option>
                                                {this.state.affectation.map((a, index) => (
                                                    <option value={a.idaffectation}>{a.projet.libelle}</option>
                                                ))}
                                            </select>
                                        </td>
                                        {this.state.cases}
                                    </tr>
                                    {this.state.newRow}
                                    <tr>
                                        <td className="taches col_fix">
                                            Absence
                                    </td>
                                        {this.state.absence}
                                    </tr>
                                    <tr>
                                        <td>Total : <br></br>
                                            {this.state.current_month} {this.state.year}</td>
                                        {this.state.total}
                                        <td>
                                            <input type="text" value={this.state.somme} className={this.state.styleTotal} maxLength="2" disabled pattern="[0-1]{1}"></input>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default calendar;
